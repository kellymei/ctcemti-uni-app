// 引入一个工具函数，用于对pages进行去重和设置首页

module.exports=(pagesJson, loader)=>{
    const hotRequire = require("uni-pages-hot-modules")(loader)

    return {
        "pages": [
            ...hotRequire("./router/index.js")
        ],
        ...pagesJson
    }
}