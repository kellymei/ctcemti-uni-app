import request from '@/utils/request'

/**
 * 版本更新接口，后台提供
 * @param {*} data 
 * @returns 
 */
export function versionUpdate(data) {
	return request.request({
		url: 'xxxx',
		method: 'post',
		data
	})
}