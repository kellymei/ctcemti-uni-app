# mti-appTabs

### 属性 Props

<table>
  <thead>
  <tr>
    <th>参数</th> 
    <th>说明</th> 
    <th>类型</th> 
    <th>是否必传</th> 
    <th>可选值</th>
    <th style="width: 180px">默认值</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>tabs</td> 
    <td>设置tabs列表数组，支持格式: ['全部', '待付款'] 或 [{name:'全部'}, {name:'待付款'}]</td> 
    <td>Array</td>
    <td>是</td>
    <td></td> 
    <td></td>
  </tr>
  <tr>
    <td>nameKey</td> 
    <td>设置取name的字段</td> 
    <td>String</td> 
    <td>否</td>
    <td></td>
    <td>name</td>
  </tr>
  <tr>
    <td>v-model</td> 
    <td>设置当前显示的下标</td> 
    <td>String | Number</td> 
    <td>否</td>
    <td></td>
    <td>0</td>
  </tr>
  <tr>
    <td>fixed</td> 
    <td>设置tabs是否悬浮</td> 
    <td>Boolean</td> 
    <td>否</td>
    <td></td>
    <td>false</td>
  </tr>
  <tr>
    <td>tabWidth</td> 
    <td>每个tab的宽度，默认不设置值，为flex平均分配; <br/>如果指定宽度，则不使用flex，每个tab居左，超过则水平滑动(单位默认rpx)</td> 
    <td>Number</td> 
    <td>否</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>height</td> 
    <td>设置tabs高度，单位(rpx)</td> 
    <td>Number</td> 
    <td>否</td>
    <td></td>
    <td>64</td>
  </tr>
  <tr>
    <td>top</td> 
    <td>顶部偏移的距离，默认单位rpx (当fixed=true时,已加上windowTop)</td> 
    <td>Number</td> 
    <td>否</td>
    <td></td>
    <td>0</td>
  </tr>
  <tr>
    <td>color</td> 
    <td>设置tabs激活状态颜色</td> 
    <td>String</td> 
    <td>否</td>
    <td></td>
    <td>'#1181FA'</td>
  </tr>
  </tbody>
</table>

### 方法 Methods

<table>
  <thead>
  <tr>
    <th style="width: 130px">方法名</th> 
    <th>说明</th> 
    <th>参数</th> 
    <th>使用方法</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>change</td> 
    <td>tab切换时触发，返回当前tab序号</td>
    <td></td> 
    <td>@change=""</td>
  </tr>
  </tbody>
</table>

#### 如使用过程中有任何问题，或者您对 mti-appTabs 插件有任何意见，请发送邮箱：1462686062@qq.com