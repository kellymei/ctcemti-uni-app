export default {
    props: {
        // 支持格式: ['全部', '待付款'] 或 [{name:'全部'}, {name:'待付款'}]
        tabs: {
            type: Array,
            default(){
                return []
            }
        },
        // 取name的字段
        nameKey: {
            type: String,
            default: 'name'
        },
        // 当前显示的下标 (使用v-model语法糖: 1.props需为value; 2.需回调input事件)
        value: {
            type: [String, Number],
            default: 0
        },
        // 是否悬浮,默认false
        fixed: Boolean,
        // 每个tab的宽度,默认不设置值,为flex平均分配; 如果指定宽度,则不使用flex,每个tab居左,超过则水平滑动(单位默认rpx)
        tabWidth: Number,
        // 高度,单位rpx
        height: { 
            type: Number,
            default: 64
        },
        // 顶部偏移的距离,默认单位rpx (当fixed=true时,已加上windowTop)
        top: { 
            type: Number,
            default: 0
        },
        // 设置tabs颜色
        color: {
            type: String,
            default: '#1181FA'
        }
    }
}
