// #ifdef APP-PLUS
let alphaBg, shareMenu, showState = false;

/**
 * 关闭弹窗
 * @returns 
 */
export const closeShare = () => {
	alphaBg && alphaBg.close();
	alphaBg && shareMenu.close();
	
	if(showState){
		showState = false;
		return true
	} else {
		showState = false;
		return false
	}
}

/**
 * 复制
 * @param {*} item 
 * @param {*} shareInfo 
 * @param {*} callback 
 */
const onCopy = (item, shareInfo,callback) => {
	const copyInfo = shareInfo.shareUrl || shareInfo.shareContent || shareInfo.shareImg;

	if (copyInfo) {
		uni.setClipboardData({
			data: copyInfo,
			success:() => {
				uni.showToast({
					title: "已复制到剪贴板",
					icon: "none"
				});
				callback && callback(item);
			}
		});
	} 
}

/**
 * 更多
 * @param {*} item 
 * @param {*} shareInfo 
 * @param {*} callback 
 */
const onMore = (item, shareInfo,callback) => {
	plus.share.sendWithSystem({
		type: "text",
		title: shareInfo.shareTitle || "",
		href: shareInfo.shareUrl || "",
		content: shareInfo.shareContent || ""
	},function(res){ 
		callback && callback(item)
	},function(err){
		console.log(err)
	})
}

/**
 * 分享
 * @param {*} item 
 * @param {*} shareInfo 
 * @param {*} callback 
 * @returns 
 */
const onShare = (item, shareInfo,callback) => {
	if (!shareInfo.type) shareInfo.type = item.type

	let shareObj = {
		provider: item.provider,
		type: shareInfo.type,
		success: (res) => {
			callback && callback(item);
			console.log(`success:${JSON.stringify(res)}`)
		},
		fail: (err) => {
			console.log(`分享失败，参数缺失 fail:${JSON.stringify(err)}`)
		}
	};

	if (shareInfo.shareTitle) {
		shareObj.title = shareInfo.shareTitle

	}else if(item.provider == "qq"){
		uni.showToast({
			title: "缺失分享的标题",
			icon: "none"
		})

		return;
	}

	if(shareInfo.type == 0 || item.provider == "qq"){
		if (shareInfo.shareUrl) {
			shareObj.href = shareInfo.shareUrl;

		}else{
			uni.showToast({
				title: "缺失分享的地址",
				icon: "none"
			})

			return;
		}
	}

	if([0,1,3,4].includes(shareInfo.type)){
		if (shareInfo.shareContent) {
			shareObj.summary = shareInfo.shareContent;
		}else{
			uni.showToast({
				title: "缺失分享的描述",
				icon: "none"
			});

			return;
		}
	}

	if([0,2,5].includes(shareInfo.type)){
		if (shareInfo.shareImg) {
			shareObj.imageUrl = shareInfo.shareImg;
		}else{
			uni.showToast({
				title: "缺失分享的图片",
				icon: "none"
			});

			return;
		}
	}

	if ([3, 4].includes(shareInfo.type)) {
		
		if (shareInfo.mediaUrl) {
			shareObj.mediaUrl = shareInfo.mediaUrl
		}else{
			uni.showToast({
				title: "缺失分享的音视频地址",
				icon: "none"
			});

			return;
		}
	}

	if (item.scene) {
		shareObj.scene = item.scene;
	}

	uni.share(shareObj)
}


const otherShareList = [
	{
		icon: "/uni_modules/mti-appShare/static/icon_copy.png",
		text: "复制",
		provider: "copy",
		onClick: onCopy
	},
	{
		icon: "/uni_modules/mti-appShare/static/icon_more.png",
		text: "更多",
		provider: "more",
		onClick: onMore
	}
];


/**
 * 获取服务商支持的分享
 */
let platformShareList = []
uni.getProvider({
	service: 'share',
	success: function (res) {
		if (res.provider.includes('sinaweibo')) {
			platformShareList = [{
				icon: "/uni_modules/mti-appShare/static/icon_weibo.png", 
				text: "微博",
				onClick: onShare,
				provider: "sinaweibo",
				type: 0
			}].concat(platformShareList);
		}

		if (res.provider.includes('qq')) {
			platformShareList = [{
				icon: "/uni_modules/mti-appShare/static/icon_qq.png",
				text: "QQ",
				onClick: onShare,
				provider: "qq",
				type: 1
			}].concat(platformShareList);
		}

		if (res.provider.includes('weixin')) {
			platformShareList = [{
				icon: "/uni_modules/mti-appShare/static/icon_weixin.png",
				text: "好友",
				onClick: onShare,
				provider: "weixin",
				scene: "WXSceneSession",
				type: 0
			},
			{
				icon: "/uni_modules/mti-appShare/static/icon_pengyouquan.png",
				text: "朋友圈",
				onClick: onShare,
				provider: "weixin",
				scene: "WXSenceTimeline",
				type: 0
			}].concat(platformShareList);
		}
	}
});

/**
 * 数据处理
 * @param {*} shareInfo 
 * @returns 
 */
const dataFactory = (shareInfo = {}) => {
	let config = {...shareInfo}

	config.shareTitle = shareInfo.shareTitle || "";
	config.shareUrl = shareInfo.shareUrl || "";
	config.shareContent = shareInfo.shareContent || "分享的描述";
	config.shareImg = shareInfo.shareImg || "分享的图片";

	return config
}

/**
 * 分享
 * @param {*} shareInfo 
 * @param {*} callback 
 * @returns 
 */
export default function (shareInfo, callback) {
	shareInfo = dataFactory(shareInfo);

	// 屏幕宽度 绘制弹框图标
	const screenWidth = plus.screen.resolutionWidth 
	const marginTop = 25, //上间距
		marginLeft = 25, //左间距
		iconWidth = 55, //图标宽宽
		iconHeight = 55, //图标高度
		icontextSpace = 10, //图标与文字间距
		textHeight = 12 //文字大小
	const colNumber = Math.floor(screenWidth / (iconWidth + marginLeft));
	const i = (screenWidth - (iconWidth + marginLeft) * colNumber - marginLeft) / (colNumber + 1);
	const initMargin = marginLeft + i;
	const itemWidth = iconWidth + initMargin;
	const itemHeight = iconHeight + icontextSpace + textHeight + marginTop;
	const textTop = iconHeight + icontextSpace;

	//先创建遮罩层
	alphaBg = new plus.nativeObj.View("alphaBg", {
		top: '0',
		left: '0',
		height: '100%',
		width: '100%',
		backgroundColor: 'rgba(0,0,0,0.5)'
	});

	//处理遮罩层点击
	alphaBg.addEventListener("click", function () { 
		alphaBg && alphaBg.close();
		shareMenu && shareMenu.close();

		showState = false;
	});

	//创建底部图标菜单
	const shareList = platformShareList.concat(otherShareList);
	shareMenu = new plus.nativeObj.View("shareMenu", {
		bottom: '0',
		left: '0',
		height: Math.ceil(shareList.length / colNumber) * itemHeight + marginTop + 44 + 1 + 'px',
		width: '100%',
		backgroundColor: '#fff'
	})

	//绘制底部图标菜单的内容
	shareMenu.draw([{
		tag: 'rect', //菜单顶部的分割灰线
		color: '#e7e7e7',
		position: {
			top: '0px',
			height: '1px'
		}
	},
	{
		tag: 'font',
		id: 'sharecancel', //底部取消按钮的文字
		text: '取消分享',
		textStyles: {
			size: '14px'
		},
		position: {
			bottom: '0px',
			height: '44px'
		}
	},
	{
		tag: 'rect', //底部取消按钮的顶部边线
		color: '#e7e7e7',
		position: {
			bottom: '45px',
			height: '1px'
		}
	}]);
	
	shareList.map((v, k) => {
		const time = new Date().getTime();
		const row = Math.floor(k / colNumber);
		const col = k % colNumber;
		const item = [{
			src: v.icon,
			id: Math.random() * 1000 + time,
			tag: "img",
			position: {
				top: row * itemHeight + marginTop,
				left: col * itemWidth + initMargin,
				width: iconWidth,
				height: iconWidth
			}
		}, {
			text: v.text,
			id: Math.random() * 1000 + time,
			tag: "font",
			textStyles: {
				size: textHeight
			},
			position: {
				top: row * itemHeight + textTop,
				left: col * itemWidth + initMargin,
				width: iconWidth,
				height: iconWidth
			}
		}];
		
		shareMenu.draw(item)
	});

	//处理底部图标菜单的点击事件，根据点击位置触发不同的逻辑
	shareMenu.addEventListener("click", function (e) { 
		if (e.screenY > plus.screen.resolutionHeight - 44) { //点击了底部取消按钮
			alphaBg && alphaBg.close();
			shareMenu && shareMenu.close();

			showState = false;
		} else if (e.clientX < 5 || e.clientX > screenWidth - 5 || e.clientY < 5) {
			//空白处点击
		} else { //点击了图标按钮
			const x = e.clientX;
			const y = e.clientY;
			const colIdx = Math.floor(x / itemWidth);
			const rowIdx = Math.floor(y / itemHeight);
			const tapIndex = colIdx + rowIdx * colNumber;

			shareList[tapIndex].onClick(shareList[tapIndex], shareInfo,callback);
		}
	});

	alphaBg.show();
	shareMenu.show();
	showState = true;

	return {
		close: function(){
			alphaBg && alphaBg.close();
			alphaBg && shareMenu.close();

			if(showState){
				showState = false;
				return true
			} else {
				showState = false;
				return false
			}
		}
	};
};
// #endif