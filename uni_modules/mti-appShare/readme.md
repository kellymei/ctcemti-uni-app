# APP分享、微博分享、QQ分享、微信好友、朋友圈

### 使用方法 第一步
在`manifest.json`文件里面的`App SDK配置`的`分享`配置对应的平台参数（`不配置参数`在自定义基座里面只会显示`复制`和`更多`， 配置之后要`重新打包`才`生效`）

### 使用方法 第二步
```
<template>
	<button type="default" @click="onShare">APP分享</button>
</template>

<script>
	// 引入方法
	import appShare, { closeShare } from 'uni_modules/mti-appShare/js_sdk/appShare';

	export default {
		methods: {
			onShare(){
				const shareData = {
					shareUrl:"https://baidu.com/",
					shareTitle:"分享的标题",
					shareContent:"分享的描述",
					shareImg:"http://xxxx.jpg",
					appPath : "pages/home/home",
					appWebUrl : "https://baidu.com/",
				};
				// 调用
				appShare(shareData,res => {
					console.log("分享成功回调", res);
					// 分享成功后关闭弹窗
					closeShare();
				});
			}
		}
	}
</script>
```
### 插件说明
| 参数名称	     | 类型 	 | 默认值	    | 描述
| -------------- |---------- | ------------ | --------------------------------------- |
| shareUrl  	 | String	 | --    | 分享的地址`（type 为 0 时必传）`         |
| shareTitle	 | String	 | --       	| 分享的标题                               |
| shareContent	 | String	 | 分享的描述	| 分享的描述`（type 为 1 时必传）`          |
| shareImg  	 | String	 | 分享的图片	| 分享的图片`（type 为 0、2 时必传）`    |
| mediaUrl	     | String	 | --	        | 分享的音视频地址`（type 为 3、4 时必传）` |
| type  	     | Number	 | 参考平台默认值| 分享形式，如图文、纯文字、纯图片、音乐、视频等，具体参考下面type说明|

### type 值说明
| 值   	  | 说明 	 | 支持平台	     | 
| ------- |--------- | ------------- |
| 0   	  | 图文 	 | 微信、新浪微博 | 
| 1   	  | 纯文字 	 | 全平台支持     |
| 2   	  | 纯图片 	 | 全平台支持     |
| 3   	  | 音乐 	 | 微信、QQ       |
| 4   	  | 视频 	 | 微信、新浪微博 |

### 平台默认值
| 平台       | 默认值 	  |
| ---------- |--------- |
| 新浪微博   | 0 	      |
| 微信好友   | 0 	      |
| 微信朋友圈 | 0 	      |
| QQ        | 1 	      |