# mti-appUpdate
# APP版本更新、强制更新、漂亮的更新界面、IOS更新（跳转IOS store）、wgt更新

### 第一步`关键`配置APP更新接口

- 目录 `api/version.js` 里面配置 `更新` 接口信息

- 目录 `config/versionConfig.js` 里面配置 `getServerNo` 请求方法

### 第二步 使用方法
``` 
// App.vue页面

// #ifdef APP-PLUS
import APPUpdate from 'uni_modules/mti-appUpdate/js_sdk/appUpdate';
// #endif

onLaunch: function(e) {
	// #ifdef APP-PLUS
	APPUpdate();
	// #endif
}
```

### 第三步 添加APP安装应用的权限
在`manifest.json`文件里面`APP模块权限配置`的`Android打包权限配置`勾选以下权限
```
<uses-permission android:name=\"android.permission.INSTALL_PACKAGES\"/>  
<uses-permission android:name=\"android.permission.REQUEST_INSTALL_PACKAGES\"/>
```

### 检查APP是否有新版本（一般在设置页面使用）
```
// #ifdef APP-PLUS
import APPUpdate, { getCurrentNo } from '@/uni_modules/zhouWei-APPUpdate/js_sdk/appUpdate';
// #endif
export default {
	data() {
		return {
			version: "" // 版本号
		};
	},
	//第一次加载
	onLoad(e) {
		// #ifdef APP-PLUS
		getCurrentNo(res => {
			// 进页面获取当前APP版本号（用于页面显示）
			this.version = res.version;
		});
		// #endif
	},
	//方法
	methods: {
		// 检查APP是否有新版本
		onAPPUpdate() {
			// true 没有新版本的时候有提示，默认：false
			APPUpdate(true);
		}
	}
}
```