// import { versionUpdate } from '@/api/version'

module.exports = {
  /**
   * 更新弹框主题色
   */
  updateColor: '0c58dc',

  /**
   * 更新图标
   */
  updateIcon: '',

  /**
   * 发起ajax请求获取服务端版本号
   * @param {*} version 
   * @param {*} isPrompt 
   * @param {*} callback 
   */
  getServerNo: (version, isPrompt = false, platform, callback) => {
    const httpData = {
      // 应用当前版本号 (自动获取)
			version: version.versionCode,  
			// 版本名称 (自动获取)
		  versionName: version.versionName,
			// setupPage参数说明 => 判断用户是不是从设置页面点击的更新，如果是设置页面点击的更新，静默更新
      setupPage: isPrompt,
      // 1101 => 安卓，1102 => IOS
      type: platform === "android" ? 1101 : 1102,
      isPrompt: isPrompt,
      updateType: "solicit"
    };
    
		versionUpdate(httpData).then(res => {
			if (res && res.downloadUrl) {
				// 兼容之前的版本（updateType是新版才有的参数）
				if(res.updateType){
					callback && callback(res);
        } else {
          res.updateType = res.forceUpdate ? "forcibly" : "solicit"

					callback && callback(res)
				}
			} else if (isPrompt) {
				uni.showToast({
					title: "暂无新版本",
					icon: "none"
				})
			}
		})
	}
}
