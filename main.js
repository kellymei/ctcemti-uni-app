import Vue from 'vue';
import App from './App';
import uView from 'uview-ui';

App.mpType = 'app';
Vue.use(uView);

const msg = (title, icon = 'none', duration = 3000, mask = false) => {
	//统一提示方便全局修改
	if (Boolean(title) === false) {
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}
const showLoading = (title = '请稍后...') => {
	uni.showLoading({title: title});
}
const hideLoading = () => {
	uni.hideLoading();
}

Vue.prototype.$api = {
	msg,
	showLoading,
	hideLoading
};

/**
 * 定义全局异常
 */
const errorHandler = (err, vm, info) => {
	console.error(`Error: ${err.toString()}\nInfo: ${info}`);
	uni.$u.toast(`Error: ${err.toString()}  Info: ${info}`);
}
Vue.config.errorHandler = errorHandler;
Vue.prototype.$throw = (error) => errorHandler(error, this);

const app = new Vue({
	...App
});

app.$mount();
